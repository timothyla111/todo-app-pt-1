import React, { Component} from "react";
import {Route, Switch, NavLink} from 'react-router-dom'
import todosList from "./todos.json";
import TodoList from "./components/todoList/TodoList"

class App extends Component {
  state = {
    todos: todosList,
    storeInput: "",
  };
  

  addItemToList = (e) => {
    if (e.key === "Enter") {
      const newToDo = {
        userId: 1,
        id: Math.floor(Math.random() * 10000),
        title: e.target.value,
        completed: false
      };
      const newToDos = 
        this.state.todos.slice();
        newToDos.push(newToDo);
        this.setState({
          todos: newToDos
        });
        e.target.value = "";
      }
  }

  toggleComplete = (id) => {
    let newToDos = this.state.todos.map(todo => {
      if(todo.id === id){
        return {
          ...todo, 
          completed: !todo.completed
        }
      }
      return todo
    }) 
    this.setState({
      todos: newToDos
    })
  }

  handleDelete = (todoId) => {
    console.log(todoId)
    const newTodos = this.state.todos.filter(
      todo => todo.id !== todoId
    )
    console.log(newTodos)
    this.setState({todos: newTodos})
  }

  clearComplete = () => {
    const removeComplete = this.state.todos.filter(
      todo => todo.completed === false 
    )
    this.setState({todos: removeComplete})
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>Todos</h1>
          <input 
            className="new-todo" 
            placeholder="What needs to be done?" 
            onKeyDown={this.addItemToList}
            autoFocus 
          />
        </header>
          <Switch>
            <Route exact path="/" render={(props) => <TodoList {...props} todos={this.state.todos} toggleComplete={this.toggleComplete} handleDelete={this.handleDelete}  />} />
            <Route exact path="/active" render={(props) => <TodoList {...props} todos={this.state.todos.filter(todo => todo.completed === false)}  toggleComplete={this.toggleComplete} handleDelete={this.handleDelete} />} />
            <Route exact path="/completed" render={(props) => <TodoList {...props} todos={this.state.todos.filter(todo => todo.completed === true)} toggleComplete={this.toggleComplete} handleDelete={this.handleDelete}  />} />
          </Switch>
 
        <footer className="footer">
          <span className="todo-count">
            <strong>{
              //I received help from Leanne Benson for the this.state.todos.filter
              this.state.todos.filter(todo => {
                  //return something true or false should show the amount left in todo list to be completed
                  if (todo.completed === true) {
                    return false;
                  }
                  return true;
                }).length
            }
            </strong> item(s) left
            </span>
          <ul className="filters">
            <li>
              {/* I now have a better understanding of NavLink and activeClassName thanks to John W */}
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={this.clearComplete}>Clear completed</button>
        </footer>

      </section>
    );
  }
}



export default App;

