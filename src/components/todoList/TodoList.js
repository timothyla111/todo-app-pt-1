import React, { Component } from 'react'
import TodoItem from '../todoItem/TodoItem'

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem key={todo.id}  
              title={todo.title} completed={todo.completed} 
              toggleComplete={e => this.props.toggleComplete(todo.id)} 
              handleDelete={e => this.props.handleDelete(todo.id)}
              />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList
